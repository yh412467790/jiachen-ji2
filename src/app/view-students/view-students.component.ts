import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import {NgForm} from '@angular/forms';
import {FormsModule} from '@angular/forms';

@Component({
  selector: 'app-view-students',
  templateUrl: './view-students.component.html',
  styleUrls: ['./view-students.component.less']
})
export class ViewStudentsComponent implements OnInit {
  list;
  private headers = new Headers({'Content-Type': 'application/json'})

  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule) { }

  // Request data
  getData() {
    this.http.get('http://globalportal.test/users').subscribe((data)=>{
      console.log(data);
      this.list = data;
    });
  }

  delete(id){
    this.http.get('http://globalportal.test/user/delete/' + id).subscribe((data)=>{
      console.log(data);
      alert('Delete Successfully');
    });
    location.reload();
  }

  ngOnInit() {
  }

}
