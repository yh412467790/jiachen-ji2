import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent} from './container/mainpage/mainpage.component';
import { LoginComponent } from './container/login/login.component';
import { ForgetpasswordComponent } from './container/forgetpassword/forgetpassword.component';
import { SignupComponent } from './container/signup/signup.component';
import { AccountsettingComponent } from './container/accountsetting/accountsetting.component';
import { CreatenewpasswordComponent } from './container/createnewpassword/createnewpassword.component';
import { VerifyemailComponent } from './container/verifyemail/verifyemail.component';
import { AirportformComponent } from './airportform/airportform.component';
import { MentorApplicationComponent } from './mentor-application/mentor-application.component';
import { registerLocaleData } from '@angular/common';
import { RegisterComponent } from './register/register.component';
import { ViewStudentsComponent } from './view-students/view-students.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { ViewEventComponent } from './view-event/view-event.component';
import { ViewpickupComponent } from './viewpickup/viewpickup.component';


const routes: Routes = [
  {path: 'main', component: MainpageComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'viewstudents', component: ViewStudentsComponent},
  {path: 'viewevents', component: ViewEventComponent},
  {path: 'viewpickups', component: ViewpickupComponent},
  {path: 'createevent', component: CreateEventComponent},
  {path: 'login', component: LoginComponent},
  {path: 'forgetpassword', component: ForgetpasswordComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'accountsetting', component: AccountsettingComponent},
  {path: 'createnewpassword', component: CreatenewpasswordComponent},
  {path: 'verifyemail', component: VerifyemailComponent},
  {path: 'airportform', component: AirportformComponent},
  {path: 'mentorapplication', component: MentorApplicationComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
