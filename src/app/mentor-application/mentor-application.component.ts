import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mentor-application',
  templateUrl: './mentor-application.component.html',
  styleUrls: ['./mentor-application.component.less']
})
export class MentorApplicationComponent implements OnInit {
  selectedFile = null;

  onFileSelected(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];
  }

  onUpload() {

  }

  constructor() { }

  // const fileInput = document.querySelector('#file-js-example input[type=file]');
  // fileInput.onchange = () => {
  //   if (fileInput.files.length > 0) {
  //     const fileName = document.querySelector('#file-js-example .file-name');
  //     fileName.textContent = fileInput.files[0].name;
  //   }
  // }

  ngOnInit() {
  }

}
