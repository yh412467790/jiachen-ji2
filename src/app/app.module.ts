import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, zh_CN, en_US } from 'ng-zorro-antd';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgForm} from '@angular/forms';



import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { MainpageComponent } from './container/mainpage/mainpage.component';
import { LoginComponent } from './container/login/login.component';
import { ForgetpasswordComponent } from './container/forgetpassword/forgetpassword.component';
import { SignupComponent } from './container/signup/signup.component';
import { AccountsettingComponent } from './container/accountsetting/accountsetting.component';
import { CreatenewpasswordComponent } from './container/createnewpassword/createnewpassword.component';
import { VerifyemailComponent } from './container/verifyemail/verifyemail.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AirportformComponent } from './airportform/airportform.component';
import { MentorApplicationComponent } from './mentor-application/mentor-application.component';
import { RegisterComponent } from './register/register.component';
import { ViewStudentsComponent } from './view-students/view-students.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { ViewEventComponent } from './view-event/view-event.component';
import { ViewpickupComponent } from './viewpickup/viewpickup.component';
registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    LoginComponent,
    ForgetpasswordComponent,
    SignupComponent,
    AccountsettingComponent,
    CreatenewpasswordComponent,
    VerifyemailComponent,
    HeaderComponent,
    FooterComponent,
    AirportformComponent,
    MentorApplicationComponent,
    RegisterComponent,
    ViewStudentsComponent,
    CreateEventComponent,
    ViewEventComponent,
    ViewpickupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HttpClientJsonpModule,
    FormsModule,
    /** 导入 ng-zorro-antd 模块 **/
    NgZorroAntdModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
