import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.less']
})
export class CreateEventComponent implements OnInit {
  list;
  private headers = new Headers({ 'Content-Type': 'application/json' })
  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule) { }

  //Post Data
  postData(f: NgForm) {

    if (f.controls['eventname'].value != "") {
      this.http.post('http://globalportal.test/event/new',
        {
          date: f.controls['date'].value,
          time: f.controls['time'].value,
          eventname: f.controls['eventname'].value,
          eventplace: f.controls['eventplace'].value,
          description: f.controls['description'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful', data);
            alert('Event Created Successfully!');
          },
          error => {
            console.log('Error', error);
            alert('Failed to create the event!');
          });
    } else {
      alert('Please enter all fields');
    }
    console.log('form:' + f.controls['eventname'].value);
  }

  ngOnInit() {
  }

}
