import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatenewpasswordComponent } from './createnewpassword.component';

describe('CreatenewpasswordComponent', () => {
  let component: CreatenewpasswordComponent;
  let fixture: ComponentFixture<CreatenewpasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatenewpasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatenewpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
