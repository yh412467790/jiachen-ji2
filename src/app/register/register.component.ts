import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {
  list;
  private headers = new Headers({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule) {

  }



  //Post Data
  postData(f: NgForm) {

    if (f.controls['firstname'].value != "") {
      this.http.post('http://globalportal.test/user/new',
        {
          firstname: f.controls['firstname'].value,
          lastname: f.controls['lastname'].value,
          studentid: f.controls['studentid'].value,
          email: f.controls['email'].value,
          password: f.controls['password'].value,
          country: f.controls['country'].value,
          department: f.controls['department'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful', data);
            alert('Student Created Successfully!');
            location.reload();
          },
          error => {
            console.log('Error', error);
            alert('Failed to create the student!');
          });
    } else {
      alert('Please enter all fields!');
    }
    console.log('form:' + f.controls['firstname'].value);
  }

  ngOnInit() {
  }

}
