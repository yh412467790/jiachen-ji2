import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-airportform',
  templateUrl: './airportform.component.html',
  styleUrls: ['./airportform.component.less']
})
export class AirportformComponent implements OnInit {
  list;
  private headers = new Headers({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule) { }

  //Post Data
  postData(f: NgForm) {

    if (f.controls['firstname'].value != "") {
      this.http.post('http://globalportal.test/pickup/new',
        {
          firstname: f.controls['firstname'].value,
          lastname: f.controls['lastname'].value,
          email: f.controls['email'].value,
          phonenumber: f.controls['phonenumber'].value,
          arrivaldate: f.controls['arrivaldate'].value,
          arrivaltime: f.controls['arrivaltime'].value,
          arrivalairport: f.controls['arrivalairport'].value,
          flightnumber: f.controls['flightnumber'].value,
          nationality: f.controls['nationality'].value,
          message: f.controls['message'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful', data);
            alert('Pickup Form Created Successfully!');
            location.reload();
          },
          error => {
            console.log('Error', error);
            alert('Failed to create the form!');
          });
    } else {
      alert('Please enter all fields!');
    }
    console.log('form:' + f.controls['firstname'].value);
  }

  ngOnInit() {
  }

}
